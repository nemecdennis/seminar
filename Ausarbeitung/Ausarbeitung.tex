\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage[section]{placeins}
\usepackage{xcolor}
\usepackage{caption}

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\newcommand{\R}{\mathbb{R}}

\begin{document}

\title{Effiziente Hardwarebeschleuniger für neuronale Netze\\}

\author{\IEEEauthorblockN{1\textsuperscript{st} Nemec Dennis }
\IEEEauthorblockA{\textit{Karlsruher Institut für Technologie} \\
Karlsruhe, Deutschland \\
unzdz@student.kit.edu}}

\maketitle

\begin{abstract}
Aufgrund der immer höher werdenden Komplexität von künstlichen neuronalen Netzen (KNN) - wie die Menge an Neuronen oder die Tiefe des Netzes - spielt die Beschleunigung dieser ein große Rolle. Verschiedene Ansätze versuchen, KNN auf verschiedenen Ebenen zu beschleunigen. Dabei minimiert das Konzept PermDNN die Dichte der Gewichtsmatrix eines KNN und bietet auf diese Optimierung zugeschnitten eine passende Hardwarearchitektur, welche einen Durchsatz von 14,74 TOPS in der Inferenzphase des KNN bietet. Die anwendungsspezifische integrierte Schaltung (ASIC) \textit{Tensor Processing Unit} beschleunigt die für das Berechnen eines KNN wichtige Matrixmultiplikation auf bis zu 420 TOPS, in dem die Matrixmultiplikation mittels eines systolischen Feldes durchgeführt wird. \\
\end{abstract}

\begin{IEEEkeywords}
cnn, dnn, hardware, efficiency, tpu, permdnn, inference, training, accelerator
\end{IEEEkeywords}

\section{Einführung}
Künstliche neuronale Netze (KNN) erfahren seit einigen Jahren immer größere Beliebtheit und Relevanz. Dem menschlichem Gehirn zum Vorbild
sind KNNs in der Lage verschiedenste Probleme effizient zu lösen. Bei einigen Problemen übertreffen sie das menschliche Gehirn, wie zum Beispiel beim Erkennen von Mustern in einer noch unbekannten Datenmenge.
So können beispielsweise KNN mit einer Genauigkeit von 91\% Krebs im Frühstadium anhand von Röntgenbildern erkennen, während erfahrene Radiologen sie nur mit 87 prozentiger Genauigkeit erkennen können \cite{ai-cancer}. Auch andere Problemstellungen können KNNs mit sehr hoher Präzision effizient lösen, wie bspw. Spracherkennung \cite{ai-speech}, Objekterkennung \cite{ai-object} oder bei der Sprachverarbeitung \cite{ai-speech-pr}. Aufgrund der Vielfalt werden KNN in heutigen Anwendungen breitgefächert eingesetzt. Insbesondere Google verwendet im großen Stil neuronale Netze für seine Dienste. Laut eigener Angabe machen die Berechnung von KNN 95\% der Rechenlast der Google-Rechenzentren aus \cite{google-util}. Aufgrund großflächiger Anwendung von KNN in der Industrie, auf mobilen End- oder vernetzen \textit{Internet of Things}-Geräten, wie zum Beispiel eine Kaffeemaschine oder der Toaster, spielt die Optimierung der KNN eine große Rolle. Bei der Anwendung auf solchen Geräten ist der Energie- und Speicherplatzbedarf ein entscheidender Punkt, da dieser in dem Anwendungsgebiet nur begrenzt verfügbar ist und beispielsweise die Akkulaufzeit nicht durch Nutzung von KNNs leiden darf. Bei großflächiger Anwendung in Rechenzentren spielt der Energiebedarf ebenso eine große Rolle. Schon die Einsparung von einigen Watt im Verhältnis zur erzielten Leistung können enorme Strommengen und dadurch hohe Kosten eingespart werden. Nicht nur der Verbrauch von Strom ist für die Anwendung von KNN wichtig, sondern auch der zeitliche Aspekt oder der Speicherplatzbedarf. Während in der Vergangenheit die Berechnung von KNN überwiegend auf CPUs stattfand, werden heutzutage Vorteile von anderen Technologien, wie die beschleunigte Matrixberechnung der GPU, genutzt, um KNN effizienter zu berechnen. Zusätzlich zur GPU kommen für die Berechnung von KNN auch weitere Beschleuniger zum Einsatz, wie beispielsweise die Tensor Processing Units (TPU).\\ \\
Das Ziel der Arbeit ist es, die verschiedenen Technologien und Architekturen der Hardwarebeschleuniger vorzustellen und miteinander zu vergleichen. Dabei werden verschiedene Arbeiten wie das PermDNN \cite{permdnn} oder PermCNN \cite{permcnn} gesichtet und die dahinter liegende Architektur vorgestellt. Neben der Hardwarebeschleunigung wird auch auf die Optimierung des theoretischen Modells von KNN eingegangen. \\
Die Arbeit beschäftigt sich anfangs in Kapitel 1 mit den Grundlagen der künstlichen neuronalen Netzen und stellt einige Arten von KNN vor, während in Kapitel 2 der Frage nachgegangen wird, wie das Modell von neuronalen Netzen in der Theorie verbessert werden kann, so dass Hardwarebeschleuniger davon profitieren. In Kapitel 3 werden die Architekturen einiger Hardwarebeschleuniger vorgestellt und miteinander verglichen. In Kapitel 4 werden einige alternativen Methoden erläutert, wie die Beschleunigung von KNN sich in Zukunft noch verändern kann. Kapitel 5 fasst die gewonnenen Erkenntnisse zusammen und schließt die Arbeit ab.\\

\section{Einführung in künstliche neuronale Netze} 
\label{sec:knn_intro}
Dieses Kapitel beschäftigt sich mit den Grundlagen von KNN. Hauptfokus wird auf den Aufbau, das Training und die Repräsentation in Computern gesetzt. Um den Umfang der Arbeit nicht unnötig zu vergrößern, wird nur auf den allgemeinen Aufbau eines KNN und zusätzlich auf das in der heutigen Zeit relevante \textit{Convolutional Neural Network (CNN)} eingegangen. \\ 

\subsection{Aufbau von künstlichen neuronalen Netzen}

Ein KNN besteht aus mindestens einer Eingabe- und Ausgabeschicht. Jede Schicht besteht aus verschiedenen Neuronen, welche jeweils mit Neuronen der darauffolgenden Schicht verbunden sind. Die Neuronen werden als Kreis dargestellt. Zwischen der Eingangs- und Ausgangsschicht können sich auch weitere Schichte befinden, die auch \textit{hidden layer} genannt werden. Ein solcher Aufbau ist in Bild \ref{fig:knn_hidden} zu sehen. Ist jedes Neuron der Schicht $\mathnormal{l}$ mit allen anderen Neuronen der Schicht $\mathnormal{l+1}$ verbunden, so nennt man dieses Konzept auch \textit{fully-connected layer}. \\


\begin{figure}[!htb]
	\includegraphics[width=\linewidth, clip]{KNNMitHiddenLayer.png}
	\centering
	\caption{KNN mit Hidden Layer bestehend aus der Eingabeschicht mit der Eingabe $\mathnormal{x_0, x_1, x_2}$ und der Ausgabeschicht mit der Ausgabe $\mathnormal{y_0, y_1, y_2}$}
	\label{fig:knn_hidden}
\end{figure}

\subsection{Mathematische Betrachtung eines KNN}
Die Eingabe des Neuron $\mathnormal{j}$ der Schicht $\mathnormal{l}$ wird mit $\mathnormal{x_{j}^{l}}$ bezeichnet. Neben der Eingabe sind auch die zugehörigen Gewichte entscheidend. Der Übersicht wegen wurden auf die Gewichte in den Bildern \ref{fig:aufbau_knn} und \ref{fig:knn_hidden} verzichtet. Das Gewicht eines Neuron der Schicht $\mathnormal{l}$ wird als $\mathnormal{w_{jk}^{l}}$ notiert. Das Gewicht wird der Kante, welche das Neuron $\mathnormal{k}$ der Schicht $\mathnormal{{l-1}}$ mit dem Neuron $\mathnormal{j}$ der Schicht $\mathnormal{l}$ verbindet, zugeordnet.
Die Ausgabe eines Neuron der Schicht l wird von der sogenannten \textit{Aktivierungsfunktion} $\mathnormal{g^l}$ erzeugt. Es können verschiedene Aktivierungsfunktionen für jeweils verschiedene Schichten benutzt werden. Die wichtigsten Eigenschaften der Aktivierungsfunktion ist die Differenzierbarkeit und die Nichtlinearität. Angenommen, die Funktion wäre linear, so wäre die Ausgabe des neuronalen Netzes eine Linearkombination aller Schichten. So wäre die Linearkombination wieder linear, folglich kann das KNN durch eine einzige Schicht beschrieben werden. Graphisch betrachtet, würde eine lineare Aktivierungsfunktion eine Punktwolke durch eine Gerade trennen, um beispielsweise Klassen von Daten zu bestimmen. In der Realität ist diese Art von Trennung allerdings nicht immer gegeben, weshalb auf nichtlineare Funktionen zurückgegriffen wird. Üblicherweise werden die Funktionen \textit{ReLU, sigmoid} und \textit{tanh} als Aktivierungsfunktion genutzt \cite{activation-function}. Die Ausgabefunktion erzeugt nun aus der gewichteten Summe der einzelnen Eingaben und Gewichte eine Ausgabe. In der Regel fasst man die Summe als $\mathnormal{z_{j}^{l} = \sum_{t=0}^{n-1}{x_{t}^{l} * w_{jt}^{l} + b_{j}^{l}}}$ zusammen. Wobei $\mathnormal{b_{j}^{l}}$ den Bias des Neuron $\mathnormal{j}$ der Schicht $\mathnormal{l}$ angibt.

Abschließend kann die Ausgabefunktion $\mathnormal{a_{j}^{l}}$ des Neuron $\mathnormal{j}$ der Schicht $\mathnormal{l}$ mathematisch wie folgt dargestellt werden:
\begin{equation}
\label{eq:neuron_output}
a_{j}^{l} = g^l(z_{j}^{l}) = g^l(\sum_{t=0}^{n-1}{x_{t}^{l}w_{jt}^{l}} + b_{j}^{l})
\end{equation}                     


\subsection{Training eines KNN}
Das Ziel des Trainings wird es sein, die Gewichte und den jeweiligen Bias so anzupassen, dass bei einer bestimmten Eingabe nur die Neuronen einen hohen Wert haben sollen, welche in der Ausgabeschicht den Wert des richtigen Neuron maximieren. Angenommen bei einer Objekterkennung soll nun das Stoppschild erkannt werden. Dann haben vereinfacht gesagt die Neuronen, welche die Kanten und das Achteck erkennen den höchsten Wert, während Neuronen, die Kreise oder andere geometrische Formen erkennen, im Vergleich einen niedrigen Wert aufweisen. Dies führt dazu, dass in der Ausgabeschicht genau das Neuron, das das Stoppschild repräsentiert, maximiert wird. \\
Um die passenden Ausgangsneuronen abhängig von der Eingabe zu maximieren, betrachtet man den Fehler, welcher das KNN erzeugt. Das Training besteht im Endeffekt nur daraus, den Fehler mithilfe des Gradientenabstiegsverfahren (GAV) zu minimieren \cite{gradient-descent}, in dem die Gewichte und das Bias dementsprechend angepasst werden. \\
Sei nun $\mathnormal{C}$ eine beliebige Kostenfunktion der Ausgabe des KNN und $\mu$ bezeichnet die Lernrate, welche angibt, wie groß die Schritte des GAV sein soll. Nun wird nach folgender Vorschrift jeweils das Gewicht $\mathnormal{w_{jk}^{l}}$ minimiert:

\begin{equation}
\label{eq:gradient_descent}
(w_{jk}^{l})_{i+1} = (w_{jk}^{l})_{i} - \mu\frac{\partial C}{\partial w_{jk}^{l}}
\end{equation}

Die Minimierung der Bias erfolgt analog. Ein geeigneter Algorithmus für die Berechnung des Gradienten ist der \textit{Backpropagation}-Algorithmus \cite{backprop} Die Formel \ref{eq:gradient_descent} kann nun mithilfe der Kettenregel umgeformt werden. 

\begin{equation}
\label{eq:backprop}
(w_{jk}^{l})_{i+1} = (w_{jk}^{l})_{i} - \mu\frac{\partial C}{\partial a_{j}^{l}}\frac{\partial a_{j}^{l}}{\partial z_{j}^{l}}\frac{\partial z_{j}^{l}}{\partial (w_{jk}^{l})_{i}}
\end{equation}

Die Kostenfunktion hängt offensichtlich von der Ausgabe $\mathnormal{a_{j}^{l}}$ (vgl. Gleichung \ref{eq:neuron_output}) ab. Die Ausgabe ist von der gewichteten Summe $\mathnormal{z_{j}^{l}}$ abhängig und diese hängt wiederum von den Gewichten $\mathnormal{w_{jk}^{l})_{i}}$ ab. Durch Formel \ref{eq:backprop} ist zudem ersichtlich, weshalb die Aktivierungsfunktion $\mathnormal{g}$ differenzierbar sein muss. Wäre sie nicht differenzierbar, könnte der Gradient nicht berechnet und das KNN somit nicht trainiert werden. Gleiche Umformung gilt für das Bias analog.\\
Neben der Anpassung der Gewichte und der Bias sind auch noch die sogenannten Hyperparameter (HP) entscheidend für die Genauigkeit der Approximation des KNN. Beispielsweise sind die Lernrate, die Aktivierungsfunktion und die Anzahl der versteckten Schichten HP. Diese Parameter werden vor dem Training des KNN gesetzt. Die Optimierung dieser Parameter wird auch \textit{Hyperparameter-Optimization} genannt. Weitere Informationen darüber sind unter \cite{hp-tuning} zu finden.

\subsection{Weitere Arten von KNN}
Das Kapitel hat bis jetzt nur die allgemeinste Art von KNN behandelt. Wie erwähnt, existieren noch viele weitere KNN, wie das CNN oder das \textit{Recurrent Neural Network (RNN)}. Eine gute Auflistung der verschiedenen Arten ist unter \cite{neural-zoo} zu finden.

\subsection{Matrixschreibweise}
Die einzelnen Schichten, die jeweiligen Gewichte und Bias werden in Matrizen zusammengefasst, um die Berechnung effizienter auf GPUs durchführen zu können. \\
Sei nun $\mathnormal{\phi(l)}$ die Anzahl der Neuronen der Schicht $\mathnormal{l}$, $\mathnormal{W_{l(l-1)}}$ die Gewichtsmatrix, $\mathnormal{Y_l}$ die Ausgabematrix, $\mathnormal{X_{l-1}}$ die Eingabematrix und $\mathnormal{B_{l}}$ die Biasmatrix. Dann gilt:

\begin{equation}
\label{eq:matrix}
Y_l = g_l(X_{l-1} * W_{l(l-1)} + B_{l})
\end{equation}

Wobei $\mathnormal{Y_l,B_{l} \in \R^{1\times\phi(l)}}$,  $\mathnormal{W_{l(l-1)} \in \R^{\phi(l-1)\times\phi(l)}}$ und $\mathnormal{X_{l-1} \in \R^{1\times\phi(l-1)}}$ Die Matrix $\mathnormal{W_{l(l-1)}}$ beinhaltet nun spaltenweise die Gewichtsvektoren. Die Gewichtsvektoren sind genau die Gewichte, die Neuron $\mathnormal{j}$ der Schicht $\mathnormal{l}$ zugeordnet werden. Die Ausgabefunktion $\mathnormal{g_l}$ wird nun komponentenweise auf die Ausgabematrix angewandt.\\
	

\section{Theoretische Optimierung von KNN}
\label{sec:th_opt_knn}
Neben der Weiterentwicklung der Hardware spielt die Kompression der KNN-Modelle unter anderem eine große Rolle. Beispielsweise durch das Teilen von Gewichten, oder die Einsparung von nicht benötigten Verbindungen (\textit{pruning}) kann die Speicher- und die Rechenkomplexität deutlich reduziert und somit die Berechnung der KNN beschleunigt werden \cite{neural-pruning}. Das Problem bei diesen Methoden ist jedoch, dass das Verhältnis der Kompression nicht immer deterministisch ist und das KNN irreguläre Zustände annehmen kann \cite{circnn}. Dies führt eine sehr kostspielige Berechnung der Indizes nach sich. Eine weitere Möglichkeit der Komprimierung ist die Veränderung der KNN-Modelle, in dem durch algebraische Eigenschaften die Struktur der Gewichtsmatrizen verändert werden \cite{permdnn}\cite{circnn}.
Im Folgenden werden zwei Ansätze vorgestellt, um die KNN algebraisch für die Berechnung  und vor allem für die zugehörige Hardwarearchitektur zu optimieren. Es ist zu beachten, dass hinter jedem vorgestellten Ansatz sich eine eigene Hardwarearchitektur befindet.

\subsection{Erster Ansatz: CirCNN}
Die Komprimierung des KNN wird bei CirCNN \cite{circnn} dadurch erreicht, dass die Gewichtsmatrizen in $\mathnormal{k}$ \textit{zyklische Matrizen (ZM)} aufgeteilt werden. Eine ZM ist eine Matrix, bei der die Spalten ausgehend von einer Spalte mit einer Permutationsvorschrift permutiert werden. Sei $\mathnormal{A\in\R^{n\times n}}$ nun eine ZM, dann hat $\mathnormal{A}$ folgende Form:

\begin{equation}
A := 
\begin{pmatrix}
a_{0} & a_{n-1} & a_{n-2} & \ldots & a_1 \\
a_{1} & a_{0} & a_{n-1} & \ldots & a_2 \\
a_{2} & a_{1} & a_{0} & \ldots & a_3 \\
& \ddots & \ddots & \ddots \\
a_{n-1} & a_{n-2} & a_{n-3} & \ldots & a_{0}
\end{pmatrix}
\end{equation}

Folglich wird die ZM aus dem Vektor 
$\mathnormal{
	a = \begin{pmatrix}
	a_0 & a_1 & \ldots & a_{n-1}
	\end{pmatrix}^\top}$
erzeugt. Die Gewichtsmatrix $\mathnormal{W}$ wird in zyklische Untermatritzen der Größe $\mathnormal{k\times k}$ partitioniert. Demzufolge existieren genau $\mathnormal{pq}$ zyklische Untermatrizen, wobei $\mathnormal{p = m / k}$ und $\mathnormal{q = n / k}$ gilt. Die Gewichtsmatrix $\mathnormal{W = [W_{ij}], i \in \{1...p\}, j \in \{1...q\}}$ nimmt somit folgende Gestalt an:

\begin{equation}
	W = \begin{pmatrix}
	W_{11} & & \ldots & & W_{1q} \\
	& & & & \\
	\vdots & & W_{ij} & & \vdots \\
	& & & & \\
	W_{p1} & & \ldots & & W_{pq} \\
	\end{pmatrix}
\end{equation}

wobei $\mathnormal{W_{ij}}$ die zyklischen Untermatrizen von W sind. Die Ausgabe Schicht $\mathnormal{l}$ wird durch folgende Vorgehensweise berechnet:

\begin{equation}
	a_{l} = W_{l}x = 
	\begin{pmatrix}
	\sum_{j=1}^{q}{W_{1j}x_{j}} \\
	\sum_{j=1}^{q}{W_{2j}x_{j}} \\
	\ldots \\
	\sum_{j=1}^{q}{W_{pj}x_{j}} \\
	\end{pmatrix}
	=
	\begin{pmatrix}
	a_{1} \\
	a_{2} \\
	\ldots \\
	a_{p}
	\end{pmatrix}
\end{equation}

Aufgrund der algebraischen Eigenschaften zyklischer Matrizen lässt sich die Operation $\mathnormal{W_{ij}x_{j}}$ sehr effizient mit der \textit{Fast Fourier Transformation (FFT)} berechnen. Die Berechnungskomplexität nimmt dadurch von $\mathnormal{O(n^2)}$ auf $\mathnormal{O(n\space log\space n)}$ ab. Wegen der erzeugenden Eigenschaft zyklischer Matrizen durch einen Vektor reduziert sich die Speicherkomplexität folglich von $\mathnormal{O(n^2)}$ auf $\mathnormal{O(n)}$. Wobei in beiden Fällen $\mathnormal{p}$ und $\mathnormal{q}$ hinreichend klein sein müssen. \\
Den Autoren zufolge erzeugt dieses Modell bei der Ausführung auf ASICs und FPGA eine höhere Energieeffizienz um den Faktor 6 bis 102. \cite{circnn}. Die Vorteile hinsichtlich der Hardware werden in Kapitel \ref{sec:hardware} erläutert. \\

\subsection{Zweiter Ansatz: PermDNN}
Die Autoren der Arbeit \cite{permdnn}  verfolgen eine Ähnliche Herangehensweise wie aus dem ersten Ansatz. Die Komprimierung erfolgt wieder durch die Umstrukturierung der Gewichtsmatrizen. Dabei werden die Gewichtsmatrizen $\mathnormal{W}$ in $\mathnormal{p \times p}$ \textit{permutierte Diagonalmatrizen} partitioniert. Die Permutationsvorschrift einer permutierten Diagonalmatrix wird durch den Permutationswert $\mathnormal{k}$ angegeben. In jeder Spalte der permutierten Diagonalmatrix befindet sich genau ein Element, dessen Zeile $\mathnormal{r}$ durch den Wert $\mathnormal{k}$ berechnet wird. Sei nun $\mathnormal{c_i}$ der Spaltenindex. \\ \\ Dann gilt für den Zeilenindex $\mathnormal{r_i}$:
\begin{equation}
\label{eq:row-computing}
	r_i = (c_i + k)\mod p
\end{equation}
Es ist nur erfoderlich, die von null verschiedenen Werte in einem Vektor $\mathnormal{q = \begin{pmatrix}
	q_0 & q_1 & \ldots & q_{mn/p-1}
	\end{pmatrix}}$ zu speichern. Ein willkürliches Gewicht $\mathnormal{w_{ij}}$ kann nun über folgende Vorschrift adressiert werden:
\begin{equation}
w_{ij}=
\begin{cases}
q_{k_l \times p + c} & wenn \quad (c+k_l)\ mod  \ p \equiv d\\
0         & sonst
\end{cases}
\end{equation}
Wobei $\mathnormal{c \equiv i}$ mod p, $\mathnormal{d \equiv j}$ mod p und $\mathnormal{l = (i/p) \times (n/p) + (j/p)}$
Die Hauptberechnung dieses Ansatzes ist das Modulo-Rechnen. Das bietet im Vergleich zu CirCNN einen Vorteil, da die Hardwareimplementierung der FFT deutlich komplexer ist als das Modulo-Rechnen. Folglich werden Berechnungskosten eingespart. Ein schon trainiertes KNN kann in die Struktur des PermDNN überführt werden, in dem das Verfahren \textit{permuted diagonal approximation} angewandt wird. Dabei werden nur die Einträge der alten Gewichtsmatrix berücksichtigt, die sich in den Position einer permutierten Diagonalmatrix befinden.

\subsection{Universal Approximation Theorem}
Die Effektivität eines Neuronalen Netzes kann durch das \textit{Universal Approximation Theorem} bestimmt werden. Dieses Theorem besagt, dass KNN beliebige stetige auf kompakten Mengen des $\mathnormal{\R^n}$ definierten Funktionen mit einem beliebigen Fehler approximieren können. Um zu folgern, dass ein umstrukturiertes KNN die gleiche Effektivität besitzt wie ein nicht umstrukturiertes KNN, ist es folglich hinreichend zu zeigen, dass das umstrukturierte KNN dieses Theorem erfüllt. Tatsächlich erfüllen sowohl CirCNN als auch PermDNN dieses Theorem. Der Beweis des PermDNN wurde jedoch noch nicht veröffentlicht und soll in Zukunft in einem technischen Bericht der Autoren erscheinen. Eine Beweisskizze des CirCNN ist unter \cite{circnn-short-proof} zu finden.\\


\section{Hardware-Beschleunigung}
\label{sec:hardware}
Die Berechnung eines KNN besteht grundsätzlich aus der Vorhersage (engl. inference) und des Lernens (engl. training). In beiden Phasen spielt die Beschleunigung der Matrixmultiplikation eine große Rolle, da diese die Kernoperation bei der Berechnung von KNN darstellt. Siehe Gleichung \ref{eq:matrix}. Im Folgenden soll die Hardwarearchitektur des PermDNN-Konzept vorgestellt werden, welche die Vorhersage eines KNN auf Hardwareebene beschleunigt. Des Weiteren werden die Tensor Processing Units (TPU) von Google vorgestellt. \\

\subsection{PermDNN-Architektur}
Wie in Kapitel 3 vorgestellt, besteht der Hauptvorteil dieses Konzepts darin, die Menge an zu speichernden Werten der Gewichtsmatrix zu minimieren. Dies hat zur Folge, dass die Gewichtsmatrizen statt im langsamen dynamischen Speicher (engl. Abkürzung DRAM) nun in einem im Vegleich zum DRAM schnelleren statischen Speicher (engl. Abkürzung SRAM) gespeichert werden können.\\

\subsubsection{Allgemeiner Aufbau der Architektur}
Die Hardwarearchitektur dieses Konzepts ist modular aufgebaut und kann somit für verschiedene Anwendungszwecke optimiert werden. Die Hauptbestandteile dieser Architektur sind die \textit{Processing Elements (PE)}, welche letztendlich die Vektor-Matrix-Multiplikation (VMM) ausführen. Die VMM erfolgt spaltenweise. Folglich ist jeder PE für genau $\mathnormal{m/N_{PE}}$ Zeilen zuständig, wobei $\mathnormal{N_{PE}}$ die Anzahl der PE angibt. Neben der PE sieht die Architektur zudem einen \textit{Activation SRAM} vor. In dem Speicher werden anfangs die Eingabe des KNN und im späteren Verlauf die Ausgabe der PE gespeichert, da die Ausgabe der Schicht $\mathnormal{l-1}$ wieder der Eingabe der Schicht $\mathnormal{l}$ entspricht. Der \textit{Activation Selector} wählt nun einen Wert $\mathnormal{x_i}$ des Eingabevektors $\mathnormal{x}$ aus und speichert diese in einer \textit{First In First Out} Warteschlange, um dann an alle PE simultan übertragen zu werden. Neben dem Activation-Selektor existiert noch der \textit{Zero-Selector}, welcher Null-Werte überspringt. Die Warteschlange ermöglicht zu jeder Zeit die verzögerungsfreie Übertragung des benötigten Wertes. Ein \textit{Act Routing Network} leitet schlussendlich die Ausgaben der PE an die richtige Stelle im Activation-SRAM weiter. Ein zentrales Steuerelement koordiniert den Ablauf. Bild \ref{fig:permdnn_architecture} verdeutlicht den Aufbau der Hardwarearchitektur des PermDNN.

\begin{figure}[!htb]
	\includegraphics[width=\linewidth, clip]{PermDNN-Engine.PNG}
	\centering
	\caption{Aufbau der PermDNN Architektur \cite{fig-2-permdnn} bestehend aus dem Aktivierungs-Speicher (Act SRAM), ein Feld aus \textit{processing elements (PE)}, ein Steuerelement und dem Routingnetzwerk, welches die Ausgabe der jeweiligen Speicherzelle im Aktivierungsspeicher zuordnet.}
	\label{fig:permdnn_architecture}
\end{figure}

Jeder PE besteht aus eigenen SRAM-Bänken, in denen die Permutationswerte und die jeweiligen Teile der Gewichtsmatrix gespeichert werden. Zudem besitzen PE $\mathnormal{N_{MUL}}$ Multiplizierer und $\mathnormal{N_{ACC}}$ Akkumulatoren, die die zugrundeliegende VMM ausführen. Dadurch, dass in der Regel mehr Akkumulatoren als Multiplizierer existieren, werden die Akkumulatoren wiederum in \textit{Akkumulator und Selektor Bänke} aufgeteilt, welche genau einem Multiplizierer zugeordnet werden. Folglich existieren $\mathnormal{N_{MUL}}$ solcher Bänke. Die Aufgabe der Akkumulator und Selektor Bänke besteht darin, für die Spalte $\mathnormal{j}$ den Index der zugehörigen Zeile auszurechnen und mit einem schon evtl. vorhandenen Ergebnis der vorherigen Spalte zu addieren. Die Berechnungvorschrift des Zeilenindex ist der Gleichung \ref{eq:row-computing} zu entnehmen. Die Ausgabe der jeweiligen Akkumulator und Selektor Bänke werden zur \textit{Activation Unit (ActU)} weitergeleitet. Die ActU besteht aus den Aktivierungsfunktionen \textit{Rectified Linear Unit (ReLU)} oder der \textit{hypertangent (tanh)}, welche je nach Bedarf eingesetzt werden können. Die Speicherbank der Gewichtsmatrix eines jeden PE wird in weitere Subbänke aufgeteilt, so dass jede Zeile einer Subbank mindestens $\mathnormal{N_{MUL}}$ Einträge enthält. Jede Zeile einer Subbank entspricht eine Spalte der Gewichtsmatrix. Beim Zugriff auf eine Subbank werden die anderen Subbanken deaktiviert, so dass der Energieverbrauch dadurch gesenkt wird.\\

\subsubsection{Vergleich PermDNN mit CirCNN}
Der Hauptvorteil von PermDNN gegenüber CirCNN besteht darin, dass bei PermDNN hauptsächlich nur Berechnungen mit reellen Zahlen durchgeführt werden, während bei CirCNN die Berechnung mit komplexen Zahlen aufgrund der Fast Fourier Transformation nötig ist. Bei einer komprimierten $\mathnormal{p\times p}$ Matrix benötigt der Ansatz von PermDNN genau $\mathnormal{p}$ reelle Multiplikationen. Bei CirCNN sind es schon $\mathnormal{p}$ komplexe Multiplikationen, dazu noch $\mathnormal{plogp}$ zusätzlich konstante komplexe Multiplikation aufgrund von FFT. Bei der gleichen Kompressionsrate führen die reellen Multiplikationen im Vergleich zur komplexen Multiplikation zu einer deutlichen Leistungssteigerung bei der Berechnung um den Faktor vier. Der Durchsatz beider Architekturen wird in \textit{Tera Operationen pro Sekunde} angegeben. Beim PermDNN liegt der Durchsatz bei 14,74 TOPS während der bei CirCNN nur bei 1,28 liegt. Der Durchsatz von PermDNN ist um den Faktor 11,51 höher. Die Energieeffizienz wird mit TOPS pro Watt (TOPS/W) angegeben. Bei PermDNN liegt diese bei 62,28 und bei CirCNN bei 16,0. Folglich ist PermDNN 3,89 Mal energieeffizienter als CirCNN.

\subsection{Tensor Processing Units (TPU)}
Die Tensor Processing Unit wurde 2015 von Google entwickelt als prognostiziert wurde, Benutzer würden durchschnittlich drei Minuten am Tag Spracherkennungsdienste in Anspruch nehmen \cite{google-util}. Bei weiterer Nutzung herkömmlicher Technologie für die Berechnung von KNN wie Central Processing Units (CPU) oder Graphics Processing Units (GPU) hätte dessen Anzahl um die Last tragen zu können, verdoppelt werden müssen. Dies wäre mit enormen Kosten- und Energieaufwand verbunden. Die TPU wurde innerhalb von 15 Monaten entwickelt und in Betrieb genommen.\\

\subsubsection{Allgemeiner Aufbau der TPU}
Die TPU wurde als domänenspezifischer Co-Prozessor konzipiert, welche den PCIe-Bus für die Kommunikation mit der CPU nutzt. Die einzelnen Bestandteile der TPU sind mit einem 256 Byte großen Pfad verbunden.

\begin{figure}[!htb]
	\includegraphics[width=\linewidth, clip]{AufbauTPU.PNG}
	\centering
	\caption{Aufbau der TPU-Architektur \cite{fig-3-tpu-architecture}. Hauptbestandteil ist die Matrix Multiplication Unit (MAC), die Ausgabe der MAC wird in Akkumulatoren gespeichert, welche wiederum mit der gewünschten Aktivierungsfuntkion verbunden sind.}
	\label{fig:tpu_architecture}
\end{figure}

Wie in Bild \ref{fig:tpu_architecture} zu sehen, ist die Hauptkomponente die Matrix-Multiplikationseinheit (MME), welche $\mathnormal{256 \times 256}$ \textit{multiplier-accumulator (MAC)} enthält, die 8 Bit Ganzzahlmultiplikation sowohl mit als auch ohne vorzeichenbehaftete Zahlen durchführen können. Die Eingabe der MME besteht aus den Gewichten, welche in einer \textit{First In First Out (FIFO)} Warteschlange zwischengespeichert werden und aus den Daten des 24 MiB großen \textit{Unified Buffer (UB)}. Die Gewichte bezieht die TPU direkt aus dem \textit{Gewichtespeicher (engl. Weight Memory)}, welcher aus 8 GiB DDR3-2133 Speicher besteht. Die Speicherbandbreite zwischen dem Gewichtespeicher und der Warteschlange beträgt \textit{30 GiB/s}. Der UB beinhaltet initial die Eingabe und im späteren Verlauf die Ausgabe der MME und der Aktivierungseinheit, welche die nicht linearen Aktivierungsfunktionen zur Verfügung stellt. Die Ausgabe der MME wird in Akkumulatoren zwischengespeichert, die insgesamt eine Größe von 4 MiB haben. Es sind 4096 Akkumulatoren mit einer Bittiefe von 32 Bit verbaut, die jeweils 256 Zeilen besitzen. \\

\subsubsection{Die Matrix-Multiplikationseinheit}
Der Grund, weshalb die Berechnung mit einer TPU derart beschleunigt wird, liegt in dem Aufbau der Matrix-Multiplikationseinheit. Dadurch, dass die Gewichtsmatrizen durchaus sehr viele Werte annehmen können, liegt es nahe, langsame Operationen wie Speicherzugriffe zu vermeiden. Durch den Aufbau der Matrix als \textit{systolischer Array} konnte die Berechnung der Matrix-Multiplikation ohne Speicherzugriffe realisiert werden. Ein systolischer Array besteht allgemein aus benachbarten \textit{Prozesselementen}, welche miteinander verbunden sind. Im Fall der MME der TPU besteht der Array aus $\mathnormal{256 \times 256}$ MACs, welche die benötigte Multiplikation und Addition jeweils ausführen. Bild \ref{fig:systolic_array_pe} verdeutlicht wie der Ablauf einer Matrixmultiplikation in einem systolischen Array abläuft. In dem Beispiel wird davon ausgegangen, dass die Gewichte initial gespeichert werden. Jedoch ist es in der Implementierung der MME so, dass die Gewichten - wie in der Grafik \ref{fig:tpu_architecture} erkennbar - von oben nach unten den einzelnen MACs durchgereicht werden, während die Eingabe von links nach rechts weitergegeben wird. Folglich nimmt die Berechnung schlussendlich einen diagonalförmigen Weg im systolischen Array. In Bild \ref{fig:systolic_array_pe} erhält $\mathnormal{PE_1}$ die Eingabe $\mathnormal{X_1}$ und berechnet $\mathnormal{Y_1 = W_1 * X_1}$ und gibt das Ergebnis anschließend an $\mathnormal{PE_2}$ weiter. $\mathnormal{PE_2}$ führt die Operation $\mathnormal{Y_1 = W_1 * X_1 + W_2 * X_1}$ aus. Schlussendlich führt $\mathnormal{PE_3}$ die Operation $\mathnormal{Y_1 = W_1 * X_1 + W_2 * X_1 + W_3 * X_1}$ aus. Das Ergebnis ist nun die gewünschte Ausgabe einer Matrixmultiplikation, welche ohne Speicherzugriffe zum Zwischenspeichern erfolgte. Der Durchsatz der MME beträgt in der ersten Version der TPU \textit{92 TeraOps/Sekunde (TOPS)}. \\

\begin{figure}[!htb]
	\includegraphics[width=\linewidth, clip]{systolic_array.png}
	\centering
	\caption{Systolischer Array bestehend aus drei MACs berechnet Matrixmultiplikation aus der Gewichtsmatrix $\mathnormal{W}$ und der Eingabe $\mathnormal{X}$ \cite{fig-4-sys-array}}
	\label{fig:systolic_array_pe}
\end{figure}

\subsubsection{Evolution der TPU}
Es wurden insgesamt drei Versionen der TPU entworfen. Die zweite Version der TPU (TPUv2) wurde insofern verändert, dass sie neben der Vorhersage auch zusätzlich zum Lernen verwendet werden kann \cite{tpu-systemarchitecture}. In der ersten Version bestand eine TPU aus nur einem Chip mit nur einem Rechenkern. Wie in Bild \ref{fig:tpu_evo} zu erkennen, besteht die TPUv2 nun aus vier Chips, welche jeweils zwei Rechenkerne beinhalten. Dabei besteht ein Rechenkern aus Skalar- Vektor- und Multiplikationseinheiten (MXU). \textit{High Bandwidth Memory} ersetzt den DDR3-Speicher, wobei jeder Rechenkern 8GB Speicher ansprechen kann. Bei der dritten Version der TPU (TPUv3) wurden lediglich der Gewichtespeicher auf 16GB pro Rechenkern erhöht. Die Veränderungen sind in Tabelle \ref{table:tpu_comparison} übersichtlich dargestellt. Eine detaillierte Veränderung der Architektur zwischen den Versionen wurde seitens Google der Öffentlichkeit nicht zugänglich gemacht.

\begin{figure}[!htb]
	\includegraphics[width=\linewidth, clip]{tpu-evo.png}
	\centering
	\caption{TPUv2 und TPUv3 mit jeweils zwei Rechenkernen und einer $\mathnormal{128\times 128}$ großen MXU Einheit, welche jeweils 8GB respektive 16GB \textit{High Bandwidth Memory} ansprechen können \cite{tpu-systemarchitecture}}
	\label{fig:tpu_evo}
\end{figure}

Die TPUs können sowohl im Einzel- als auch im Clusterbetrieb verwendet werden. Im Clusterbetrieb werden mehrere TPUs miteinander über eine Netzwerkschnittstelle verbunden. In der zweiten Version können somit insgesamt 512 Kerne und 4 TiB Gesamtspeicher erzeugt werden, während es in der dritten Version schon 2048 Kerne und insgesamt 32 TiB Gesamtspeicher sind. Die TPU-Cluster werden auch TPU-Pod genannt. Die Leistung eines TPUv2 Pod beträgt insgesamt 11,5 PFLOPS mit 4 TiB HBM-Speicher. Die dritte Version steigert die Leistung eines TPUv3-Pods auf approximiert 100 PFLOPS.

\begin{table}[!htb]
\caption{Vergleich der einzelnen TPU-Versionen. TFLOP bezeichnet dabei die \textit{Floating-Point Operation per Second}}
\begin{tabular}{ |p{1cm}||p{2cm}||p{2cm}||p{2cm}| }
	\hline
	\multicolumn{4}{|c|}{Evolution der TPU} \\
	\hline
	Version & TOPS & Speicher & Anzahl MXU\\
	\hline
	TPUv1 & 92 TOPS & 8GB DDR3 & 1\\
	TPUv2 & 180 TFLOPS & 64GB HBM & 2\\
	TPUv3 & 420 TFLOPS & 128GB HBM & 4\\
	\hline
\end{tabular}
\label{table:tpu_comparison}
\end{table}

\section{Zusammenfassung}
Um bestmögliche Leistung zu erzielen, spielt neben der Verbesserung der Hardwarearchitektur auch die Optimierung der einzelnen theoretischen Modelle eine große Rolle, um die Rechen- und Speicherkomplexität zu verbessern. Wie in Kapitel \ref{sec:knn_intro} dargestellt, ist die Hauptoperation bei der Berechnung eines KNN die Matrixmultiplikation. Die theoretischen Ansätze zur Optimierung zielen darauf ab, die Dichte der Einträge der Gewichtsmatrix zu minimieren. So benutzt beispielsweise PermDNN die Eigenschaft von permutierten Diagonalmatrizen aus, um die Einträge der Gewichtsmatrix zu verringern. \\ Bei der TPU wiederum wird nicht das Modell eines KNN optimiert, sondern die Matrixmultiplikation seitens der Hardware beschleunigt. Durch das Nutzen eines systolischen Arrays kann die Berechnung eines KNN sogar auf 420 TFLOPS beschleunigt werden und bietet eine kostengünstigere und schnellere Alternative im Vergleich zu GPUs, KNN zu inferieren oder zu trainieren. \cite{tpu-google} \\ \\

\begin{thebibliography}{00}
\bibitem{ai-cancer} Kim, Hyo-Eun et al ``Changes in cancer detection and false-positive recall in mammography using artificial intelligence: a retrospective, multireader study'' The Lancet Digital Health, Volume 2, Issue 3, e138 - e148, February 2020
\bibitem{ai-speech} L. Deng, G. Hinton, and B. Kingsbury, “New types of deep neural
network learning for speech recognition and related applications: An
overview,” in Acoustics, Speech and Signal Processing (ICASSP), 2013
IEEE International Conference on. IEEE, 2013.
\bibitem{ai-object} J. Deng, W. Dong, R. Socher, L.-J. Li, K. Li, and F.-F. Li, “Imagenet: A
large-scale hierarchical image database,” in Computer Vision and Pattern
Recognition, 2009. CVPR 2009. IEEE Conference on. IEEE, 2009.
\bibitem{ai-speech-pr} R. Collobert and J. Weston, “A unified architecture for natural language
processing: Deep neural networks with multitask learning,” in Proceedings
of the 25th international conference on Machine learning. ACM,
2008.
\bibitem{google-util} Norman P. Jouppi, Cliff Young, Nishant Patil, David Patterson, et al. 2017. In-Datacenter Performance Analysis of a Tensor Processing Unit. SIGARCH Comput. Archit. News 45, 2 (May 2017), 1–12. DOI:https://doi.org/10.1145/3140659.3080246
\bibitem{permdnn} C. Deng, S. Liao, Y. Xie, K. K. Parhi, X. Qian and B. Yuan, "PermDNN: Efficient Compressed DNN Architecture with Permuted Diagonal Matrices," 2018 51st Annual IEEE/ACM International Symposium on Microarchitecture (MICRO), Fukuoka, 2018, pp. 189-202, doi: 10.1109/MICRO.2018.00024.
\bibitem{permcnn} C. Deng, S. Liao and B. Yuan, "PERMCNN: Energy-efficient Convolutional Neural Network Hardware Architecture with Permuted Diagonal Structure," in IEEE Transactions on Computers, doi: 10.1109/TC.2020.2981068.
\bibitem{activation-function} Ayyüce Kızrak, "Comparison of Activation Functions for Deep Neural Networks", 2019, towards data science, https://towardsdatascience.com/comparison-of-activation-functions-for-deep-neural-networks-706ac4284c8a
\bibitem{gradient-descent} Simon Du, Jason Lee, Haochuan Li, Liwei Wang, Xiyu Zhai ; Proceedings of the 36th International Conference on Machine Learning, PMLR 97:1675-1685, 2019
\bibitem{backprop} Michael Nielsen "Neural Networks and Deep Learning", 2015, http://neuralnetworksanddeeplearning.com/chap2.html
\bibitem{hp-tuning} Bergstra, James ; Bardenet, Rémi ; Bengio, Yoshua ; Kégl, Balázs: Algorithms for Hyper-parameter Optimization. In: Proceedings of the 24th International Conference on Neural Information Processing Systems. USA : Curran Associates Inc., 2011 (NIPS'11). - ISBN 978-1-61839-599-3, S. 2546--2554
\bibitem{neural-zoo} Fjodor Van Veen, 2016, The Neural Network Zoo. The Asimov Institute, https://www.asimovinstitute.org/neural-network-zoo/
\bibitem{neural-pruning} Kimessha Paupamah, Steven James, Richard Klein, 'Quantisation and Pruning for Neural Network Compression and Regularisation', 2020
\bibitem{circnn} Caiwen Ding, Siyu Liao, Yanzhi Wang, Zhe Li, Ning Liu, Youwei Zhuo, Chao Wang, Xuehai Qian, Yu Bai, Geng Yuan, Xiaolong Ma, Yipeng Zhang, Jian Tang, Qinru Qiu, Xue Lin, and Bo Yuan. 2017. CirCNN: accelerating and compressing deep neural networks using block-circulant weight matrices. In Proceedings of the 50th Annual IEEE/ACM International Symposium on Microarchitecture (MICRO-50 ’17). Association for Computing Machinery, New York, NY, USA, 395–408. DOI:https://doi.org/10.1145/3123939.3124552
\bibitem{circnn-short-proof} Caiwen Ding, Siyu Liao, et. al. CirCNN Short Proof. https://drive.google.com/file/d/0B19Xkz1gXlwAYjVjWC1Kc2xSRm8/view
\bibitem{tpu-systemarchitecture} Systemarchitektur | Cloud TPU | Google Cloud. Mai 2020. https://cloud.google.com/tpu/docs/system-architecture
\bibitem{tpu-google} An in-depth look at Google's first Tensor Processing Unit (TPU) | Google Cloud Blog. Stand Juli 2020. https://cloud.google.com/blog/products/gcp/an-in-depth-look-at-googles-first-tensor-processing-unit-tpu
\bibitem{fig-2-permdnn} Abbildung 2: Aufbau der PermDNN-Architektur.  C. Deng, S. Liao, Y. Xie, K. K. Parhi, X. Qian and B. Yuan, "PermDNN: Efficient Compressed DNN Architecture with Permuted Diagonal Matrices," 2018 51st Annual IEEE/ACM International Symposium on Microarchitecture (MICRO), Fukuoka, 2018, p. 196, doi: 10.1109/MICRO.2018.00024. 
\bibitem{fig-3-tpu-architecture} Norman P. Jouppi, Cliff Young, Nishant Patil, David Patterson, et al. 2017. In-Datacenter Performance Analysis of a Tensor Processing Unit. SIGARCH Comput. Archit. News 45, 2 (May 2017), p. 3. DOI:https://doi.org/10.1145/3140659.3080246
\bibitem{fig-4-sys-array} Tang Shan. Systolic Array-Reborn due to Google TPU (chinesich). 2020. https://zhuanlan.zhihu.com/p/26522315

\end{thebibliography}
\end{document}
